<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Document</title>
</head>
<body>



<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>ИИН</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Возраст</th>
    </tr>
    </thead>
    <tbody>

    @foreach($data as $customer)
        <tr>
            <td>{{ $customer->iin }}</td>
            <td>{{ $customer->name }}</td>
            <td>{{ $customer->surname }}</td>
            <td>{{ $customer->age }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<hr>
<div class="col-md-5" align="right">
    <a href="{{ url('getdata/pdf') }}" class="btn btn-primary">PDF</a>
</div>

</body>
</html>