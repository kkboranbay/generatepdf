<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class PdfController extends Controller
{
    function index()
    {
        $data = $this->get_data();
        return view('pdf')->with('data', $data);
    }

    function get_data()
    {
        $data = DB::table('customer')->get();
        return $data;
    }


    function pdf()
    {

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_data());
        return $pdf->stream();
    }

    function convert_data()
    {
        $data = $this->get_data();
        $output = '
     <table width="100%">
        <tr>
            <th style="border: 1px solid">IIN</th>
            <th style="border: 1px solid;">Name</th>
            <th style="border: 1px solid;">Surname</th>
            <th style="border: 1px solid;">Age</th>
        </tr>
     ';
        foreach($data as $customer)
        {
            $output .= '
          <tr>
           <td style="border: 1px solid">'.$customer->iin.'</td>
           <td style="border: 1px solid">'.$customer->name.'</td>
           <td style="border: 1px solid">'.$customer->surname.'</td>
           <td style="border: 1px solid">'.$customer->age.'</td>
          </tr>
      ';
        }
        $output .= '</table>';
        return $output;
    }


}
